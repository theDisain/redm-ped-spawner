function WhenKeyJustPressed(key)
    if IsControlJustPressed(0, key) then
        return true
    else
        return false
    end
end

RegisterCommand('doctor', function()
    SendNUIMessage({
        action = 'setVisible',
        data = true
    })
    SetNuiFocus(true, true)
end)

RegisterNUICallback('getClientData', function(data, cb)
    model = 'mp_male'

    if data.modelValue then

        model = data.modelValue
    end

    local modelHash = GetHashKey(model)

    TriggerEvent('chat:addMessage', {
		    args = { 'chosen model: ' .. model }
	    })


    if IsModelInCdimage(modelHash) and IsModelValid(modelHash) then

        TriggerEvent('chat:addMessage', {
		    args = { 'Changing skin!' }
	    })

        RequestModel(modelHash)

        while not HasModelLoaded(modelHash) do
            Wait(5)
        end
        
        if HasModelLoaded(modelHash) then
            SetPlayerModel(PlayerId(), modelHash)
            TriggerEvent('chat:addMessage', {
		        args = { 'model '.. model ..' loaded!' }
	        })
            Citizen.InvokeNative(0x77FF8D35EEC6BBC4,PlayerPedId(),0,0)

            SetModelAsNoLongerNeeded(modelHash)
        end
    end

    cb({})

    SendNUIMessage({
        action = 'setVisible',
        data = false
    })
    SetNuiFocus(false, false)
end)


RegisterNUICallback('hideUI', function(_, cb)
    cb({})
    SetNuiFocus(false, false)
end)

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        if WhenKeyJustPressed(keys["UP"]) then
            SendNUIMessage({
                action = 'setVisible',
                data = true
            })
            SetNuiFocus(true, true)
        end
    end
end)